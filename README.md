Fork of
[gitlab.com/antora/antora-ui-default](https://gitlab.com/antora/antora-ui-default)
customised for the look-and-feel of GNSS Data suite of applications.

Nix {#_nix}
===

You could optionally prepare the build environment using the [Nix
package manager](https://nixos.org/nix).

1\) Install Nix

    $ curl https://nixos.org/nix/install | sh

2\) Enter nix shell to download and install the required software as
specified in `./shell.nix`

    $ nix-shell

Original README follows below {#_original_readme_follows_below}
=============================

![CI Status (GitLab
CI)](https://gitlab.com/antora/antora-ui-default/badges/master/pipeline.svg)

This project is an archetype that demonstrates how to produce a UI
bundle for use in a documentation site generated with
[Antora](https://antora.org). You can see a preview of the default UI at
[antora.gitlab.io/antora-ui-default](https://antora.gitlab.io/antora-ui-default).

Use the Default UI {#_use_the_default_ui}
==================

If you want to use the default UI for your Antora-generated site, add
the following UI configuration to your playbook:

``` {.yaml}
ui:
  bundle:
    url: https://gitlab.com/antora/antora-ui-default/-/jobs/artifacts/master/raw/build/ui-bundle.zip?job=bundle-stable
    snapshot: true
```

Read on to learn how to use your own build of the default UI.

Quickstart {#_quickstart}
==========

This section offers a basic tutorial for learning how to preview the
default UI and bundle it for use with Antora. A more comprehensive
tutorial will be made available in the documentation.

Prerequisites {#_prerequisites}
-------------

To preview and bundle the default UI, you need the following software on
your computer:

-   [git](https://git-scm.com) (command: `git`)

-   [Node](https://nodejs.org) (command: `node`)

-   [Gulp CLI](http://gulpjs.com) (command: `gulp`)

-   [Yarn](https://yarnpkg.com) (command: `yarn`)

### git {#_git}

First, make sure you have git installed.

    $ git --version

If not, [download and install](https://git-scm.com/downloads) the git
package for your system.

### Node {#_node}

Next, make sure that you have Node.js (herein "Node") installed.

    $ node --version

If this command fails with an error, you don't have Node installed. If
the command doesn't report a Node LTS version (e.g., v10.14.2), you
don't have a suitable version of Node installed.

While you can install Node from the official packages, we strongly
recommend that you use [nvm](https://github.com/creationix/nvm) (Node
Version Manager) to install and manage Node. Follow the [nvm
installation
instructions](https://github.com/creationix/nvm#installation) to set up
nvm on your machine.

Once you've installed nvm, open a new terminal and install Node 10 using
the following command:

    $ nvm install 10

You can switch to this version of Node at any time using the following
command:

    $ nvm use 10

To make Node 10 the default in new terminals, type:

    $ nvm alias default 10

Now that you have Node 10 installed, you can proceed with installing the
Gulp CLI and Yarn.

### Gulp CLI {#_gulp_cli}

Next, you'll need the Gulp command-line interface (CLI). This package
provides the `gulp` command which executes the version of Gulp declared
by the project.

You should install the Gulp CLI globally (which resolves to a location
in your user directory if you're using nvm) using the following command:

    $ npm install -g gulp-cli

### Yarn {#_yarn}

Finally, you'll need Yarn, which is the preferred package manager for
the Node ecosystem.

You should install Yarn globally (which resolves to a location in your
user directory if you're using nvm) using the following command:

    $ npm install -g yarn

Now that you have the prerequisites installed, you can fetch and build
the default UI project.

Clone and Initialize the UI Project {#_clone_and_initialize_the_ui_project}
-----------------------------------

Clone the default UI project using git:

    $ git clone https://gitlab.com/antora/antora-ui-default &&
      cd "`basename $_`"

The example above clones Antora's default UI project and then switches
to the project folder on your filesystem. Stay in this project folder in
order to initialize the project using Yarn.

Use Yarn to install the project's dependencies. In your terminal,
execute the following command (while inside the project folder):

    $ yarn install

This command installs the dependencies listed in *package.json* into the
*node\_modules/* folder inside the project. This folder does not get
included in the UI bundle.

Preview the UI {#_preview_the_ui}
--------------

The default UI project is configured to preview offline. That's what the
files in the *preview-src/* folder are for. This folder contains HTML
file fragments that provide a representative sample of content from the
site.

To build the UI and preview it in a local web server, run the `preview`
command:

    $ gulp preview

You'll see a URL listed in the output of this command:

    [12:59:28] Starting 'preview:serve'...
    [12:59:28] Starting server...
    [12:59:28] Server started http://localhost:5252
    [12:59:28] Running server

Navigate to that URL to view the preview site.

While this command is running, any changes you make to the source files
will be instantly reflected in the browser. This works by monitoring the
project for changes, running the `preview:build` task if a change is
detected, and sending the updates to the browser.

Press [Ctrl+C]{.keycombo} to stop the preview server and end the
continuous build.

Package for Use with Antora {#_package_for_use_with_antora}
---------------------------

If you need to bundle the UI in order to preview the UI on the real site
in local development, run the following command:

    $ gulp bundle

The UI bundle will be available at *build/ui-bundle.zip*. You can then
point Antora at this bundle using the `--ui-bundle-url` command-line
option.

If you have the preview running, and you want to bundle without causing
the preview to be clobbered, use:

    $ gulp bundle:pack

The UI bundle will again be available at *build/ui-bundle.zip*.

Copyright and License {#_copyright_and_license}
=====================

Copyright © 2017-2018 OpenDevise Inc. and the Antora Project.

Use of this software is granted under the terms of the [Mozilla Public
License Version 2.0](https://www.mozilla.org/en-US/MPL/2.0/) (MPL-2.0).
See [LICENSE](LICENSE) to find the full license text.

Authors {#_authors}
=======

Development of Antora is led and sponsored by [OpenDevise
Inc](https://opendevise.com).

#!/usr/bin/env bash

# Read asciidoc markup from stdin and output markdown markup to stdout.

# turn on "strict bash"
set -eou pipefail

asciidoctor -b docbook - -o - | pandoc --from=docbook --to=markdown

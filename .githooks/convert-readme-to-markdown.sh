#!/usr/bin/env bash

# turn on strict bash
set -euo pipefail

if [[ $(git diff --cached README.adoc) ]]; then
    ./scripts/asciidoc2markdown.sh < README.adoc > README.md
    git add README.md
fi

let

  pkgs = import ./nix {};

  devEnv = with pkgs; buildEnv {
    name = "devEnv";
    paths = [
      asciidoctor
      autoconf
      automake
      awscli
      curl
      git
      nodejs-10_x
      nodePackages_10_x.gulp
      nodePackages_10_x.yarn
      pandoc
      zlib
      zlib.dev
    ];
  };

in

  pkgs.mkShell rec {
    buildInputs = [
      devEnv
    ];

    LD_LIBRARY_PATH = pkgs.lib.makeLibraryPath buildInputs;

    shellHook = ''
      # https://github.com/imagemin/optipng-bin/issues/108
      export LD="$CC"
    '';
  }
